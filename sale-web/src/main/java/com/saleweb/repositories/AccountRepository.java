package com.saleweb.repositories;

import com.saleweb.domains.Account;
import com.saleweb.models.account.AccountQueryDTO;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.Optional;

public interface AccountRepository
    extends PagingAndSortingRepository<Account, Long>, JpaSpecificationExecutor<Account> {
    Boolean existsByUserName(String username);

    Optional<Account> findByUserName(String userName);

  @Query(
      value =
          "select new com.saleweb.models.account.AccountQueryDTO(ac.id,ac.userName,ac.password,ac.status,rl.id,rl.name,us.fullName,us.address,us.email,us.phoneNumber,us.dateOfBirth,us.gender)\n"
              + "from Account ac\n"
              + "left join User us on us.id=ac.userId \n"
              + "left join Role rl on rl.id=ac.roleId \n"
              + "where ac.userName=:userName \n"
              + "and ac.password=:password")
 AccountQueryDTO getByUsernameAndPassword(String userName, String password);
}
