package com.saleweb.repositories;

import com.saleweb.domains.OrderStatus;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface OrdersStatusRepository
    extends PagingAndSortingRepository<OrderStatus, Long>, JpaSpecificationExecutor<OrderStatus> {}
