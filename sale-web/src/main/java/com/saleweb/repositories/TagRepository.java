package com.saleweb.repositories;

import com.saleweb.domains.Tag;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface TagRepository
    extends PagingAndSortingRepository<Tag, Long>, JpaSpecificationExecutor<Tag> {}
