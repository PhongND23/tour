package com.saleweb.repositories;

import com.saleweb.domains.PostCategory;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface PostCategoryRepository
    extends PagingAndSortingRepository<PostCategory, Long>,
        JpaSpecificationExecutor<PostCategory> {}
