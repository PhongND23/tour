package com.saleweb.repositories;

import com.saleweb.domains.Cart;
import com.saleweb.domains.Order;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.Optional;

public interface CartRepository
    extends PagingAndSortingRepository<Cart, Long>, JpaSpecificationExecutor<Cart> {

    Optional<Cart> findByAccountId(Long id);
}
