package com.saleweb.repositories;

import com.saleweb.domains.Cart;
import com.saleweb.domains.CartDetail;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;
import java.util.Optional;

public interface CartDetailRepository
    extends PagingAndSortingRepository<CartDetail, Long>, JpaSpecificationExecutor<CartDetail> {
    Boolean existsByCartIdAndProductId(Long cartId,Long productId);
    Optional<CartDetail> findByCartIdAndProductId(Long cartId, Long productId);
    List<CartDetail> findByIdIn(List<Long> ids);
    List<CartDetail> findByCartId(Long id);
}
