package com.saleweb.repositories;

import com.saleweb.domains.Product;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface ProductRepository
    extends PagingAndSortingRepository<Product, Long>, JpaSpecificationExecutor<Product> {

  List<Product> findAllByOrderByCreatedDateDesc();

  List<Product> findAllByOrderByViewCountDesc();

  List<Product> findByIdIn(List<Long> ids);
}
