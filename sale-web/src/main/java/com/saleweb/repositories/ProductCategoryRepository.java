package com.saleweb.repositories;

import com.saleweb.domains.ProductCategory;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface ProductCategoryRepository
    extends PagingAndSortingRepository<ProductCategory, Long>,
        JpaSpecificationExecutor<ProductCategory> {}
