package com.saleweb.models.account;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.saleweb.domains.Account;
import com.saleweb.models.role.RoleDTO;
import com.saleweb.models.user.UserDTO;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class AccountDTO {

  private Long id;

  private String userName;

  private String password;

  private Long roleId;

  private RoleDTO roleDTO;

  private Long userId;

  private UserDTO userDTO;
  @JsonProperty private Boolean status;

  public AccountDTO(Account account, RoleDTO roleDTO, UserDTO userDTO) {
    this.setId(account.getId());
    this.setUserName(account.getUserName());
    this.setPassword(account.getPassword());
    this.setRoleId(account.getRoleId());
    this.setRoleDTO(roleDTO);
    this.setUserId(account.getUserId());
    this.setUserDTO(userDTO);
    this.setStatus(account.getStatus());
  }

  public AccountDTO(Account account) {
    this.setId(account.getId());
    this.setUserName(account.getUserName());
    this.setPassword(account.getPassword());
    this.setRoleId(account.getRoleId());
    this.setUserId(account.getUserId());
    this.setStatus(account.getStatus());
  }
}
