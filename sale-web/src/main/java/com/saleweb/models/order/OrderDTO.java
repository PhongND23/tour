package com.saleweb.models.order;

import com.saleweb.models.cart.ProductCartDTO;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class OrderDTO {

  private Long id;

  private Long accountId;

  private String fullName;
  private String address;
  private String email;
  private String phoneNumber;
  private LocalDate dateOfBirth;
  private Double ship;

  private String orderStatusName;
  private Long orderStatusId;
  private String paymentName;
  private Long paymentId;
  private String productsString;
  private List<ProductCartDTO> products;
  private Double total;
  private LocalDateTime createdDate;

  public OrderDTO(
      Long id,
      Long accountId,
      String fullName,
      String address,
      String email,
      String phoneNumber,
      LocalDate dateOfBirth,
      Double ship,
      String orderStatusName,
      Long orderStatusId,
      String paymentName,
      Long paymentId,
      String products,
      LocalDateTime createdDate) {

    this.setId(id);
    this.setAccountId(accountId);
    this.setFullName(fullName);
    this.setAddress(address);
    this.setEmail(email);
    this.setPhoneNumber(phoneNumber);
    this.setDateOfBirth(dateOfBirth);
    this.setShip(ship);
    this.setOrderStatusName(orderStatusName);
    this.setOrderStatusId(orderStatusId);
    this.setPaymentName(paymentName);
    this.setPaymentId(paymentId);
    this.setProductsString(products);
    this.setCreatedDate(createdDate);
  }
}
