package com.saleweb.models.category;

import com.saleweb.domains.PostCategory;
import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
public class PostCategoryDTO {
  private Long id;
  private String name;
  private String description;
  private Boolean status;
  public PostCategoryDTO(PostCategory postCategory)
  {
    this.setId(postCategory.getId());
    this.setName(postCategory.getName());
    this.setDescription(postCategory.getDescription());
    this.setStatus(postCategory.getStatus());
  }

}
