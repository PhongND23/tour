package com.saleweb.models.category;

import com.saleweb.domains.OrderStatus;
import com.saleweb.domains.PostCategory;
import com.saleweb.domains.ProductCategory;
import com.saleweb.domains.Role;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class DropDownDTO {
    private Long id;
    private String name;

    public DropDownDTO(ProductCategory domain)
    {
        this.setId(domain.getId());
        this.setName(domain.getName());
    }

    public DropDownDTO(PostCategory domain)
    {
        this.setId(domain.getId());
        this.setName(domain.getName());
    }

    public DropDownDTO(Role domain)
    {
        this.setId(domain.getId());
        this.setName(domain.getName());
    }

    public DropDownDTO(OrderStatus domain)
    {
        this.setId(domain.getId());
        this.setName(domain.getName());
    }
}
