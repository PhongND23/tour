package com.saleweb.models.category;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
public class ProductCategoryUpdateDTO {
  private String name;

  private String alias;

  private String description;

  private Integer displayOrder;

  private String image;

  private Boolean isShowHome;

  private Boolean status;
}
