package com.saleweb.models.category;

import com.saleweb.domains.ProductCategory;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
public class ProductCategoryDTO {

  private String name;

  private String description;

  private Boolean status;

  public ProductCategoryDTO(ProductCategory domain) {
    this.name = domain.getName();
    this.description = domain.getDescription();
    this.status = domain.getStatus();
  }
}
