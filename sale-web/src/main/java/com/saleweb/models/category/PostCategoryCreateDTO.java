package com.saleweb.models.category;

import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
public class PostCategoryCreateDTO {
  private String name;
  private String description;

}
