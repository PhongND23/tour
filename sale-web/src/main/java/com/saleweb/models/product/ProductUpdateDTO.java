package com.saleweb.models.product;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
public class ProductUpdateDTO {

  private String name;

  private Long categoryId;

  private List<String> images;

  private Double price;

  private Double promotionPrice;

  private Integer quantity;

  private String description;

  private String content;

  @JsonProperty
  private Boolean status;

  private String classify;

  private String trademark;

  @JsonProperty
  private Boolean isHot;

  private String tags;


}
