package com.saleweb.models.product;

import com.saleweb.domains.Product;
import lombok.*;

import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
public class ProductCreateDTO {

  private String name;

  private Long categoryId;

  private List<String> images;

  private Double price;

  private Double promotionPrice;

  private Integer quantity;

  private String description;

  private String content;

  private Integer viewCount;

  private Boolean status;

  private String classify;

  private String trademark;

  private Long id;

  private String tags;


}
