package com.saleweb.models.post;

import com.saleweb.domains.Post;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
public class PostCreateDTO {

  private String title;

  private Long categoryId;

  private List<String> images;

  private String sortDescription;;

  private String content;

  private Boolean isShowHome;

  private Boolean isHot;

  private Integer viewCount;

  private Integer likeCount;

  private Boolean status;

  private String tags;

}
