package com.saleweb.models.post;

import com.saleweb.domains.Post;
import lombok.*;

import java.time.LocalDateTime;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
public class PostDTO {
  private Long id;

  private String title;

  private Long categoryId;

  private String categoryName;

  private List<String> images;

  private String content;

  private Boolean isHot;

  private Integer viewCount;

  private Integer likeCount;

  private Boolean status;

  private String tags;
  private String sortDescription;
  private LocalDateTime createdDate;
  public PostDTO(Post post,List<String> images,String categoryName)
  {
    this.setId(post.getId());
    this.setTitle(post.getTitle());
    this.setCategoryId(post.getCategoryId());
    this.setCategoryName(categoryName);
    this.setImages(images);
    this.setContent(post.getContent());
    this.setIsHot(post.getIsHot());
    this.setViewCount(post.getViewCount());
    this.setLikeCount(post.getLikeCount());
    this.setStatus(post.getStatus());
    this.setTags(post.getTags());
    this.setCreatedDate(post.getCreatedDate());
    this.setSortDescription(post.getSortDescription());
  }

}
