package com.saleweb.models.role;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class RoleUpdateDTO {

    private String name;

    @JsonProperty
    private Boolean status;

    private String description;

}
