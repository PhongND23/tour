package com.saleweb.controllers;

import com.saleweb.common.MessageDTO;
import com.saleweb.common.SearchDTO;
import com.saleweb.domains.Tag;
import com.saleweb.exceptions.ApplicationException;
import com.saleweb.exceptions.ExceptionUtils;
import com.saleweb.services.TagService;
import io.swagger.v3.oas.annotations.Parameter;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RequiredArgsConstructor
@RequestMapping("/tag")
@RestController
public class TagController {
  private final TagService service;

  @CrossOrigin
  @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<Object> create(@RequestBody Tag dto) {
    try {
      service.create(dto);
      return new ResponseEntity<>(HttpStatus.CREATED);
    } catch (ApplicationException ex) {
      return new ResponseEntity<>(new MessageDTO(ex.getMessage()), HttpStatus.BAD_REQUEST);
    } catch (Exception ex) {
      return new ResponseEntity<>(
          new MessageDTO(ExceptionUtils.E_INTERNAL_SERVER), HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @CrossOrigin
  @PutMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<Object> update(@PathVariable Long id, @RequestBody Tag dto) {
    try {
      service.update(dto, id);
      return new ResponseEntity<>(HttpStatus.ACCEPTED);
    } catch (ApplicationException ex) {
      return new ResponseEntity<>(new MessageDTO(ex.getMessage()), HttpStatus.BAD_REQUEST);
    } catch (Exception ex) {
      return new ResponseEntity<>(
          new MessageDTO(ExceptionUtils.E_INTERNAL_SERVER), HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @CrossOrigin
  @DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<Object> delete(@PathVariable Long id) {
    try {
      service.delete(id);
      return new ResponseEntity<>(HttpStatus.OK);
    } catch (ApplicationException ex) {
      return new ResponseEntity<>(new MessageDTO(ex.getMessage()), HttpStatus.BAD_REQUEST);
    } catch (Exception ex) {
      return new ResponseEntity<>(
          new MessageDTO(ExceptionUtils.E_INTERNAL_SERVER), HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @CrossOrigin
  @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<Object> findById(@PathVariable Long id) {
    try {
      return new ResponseEntity<>(service.findById(id), HttpStatus.OK);
    } catch (Exception ex) {
      return new ResponseEntity<>(
          new MessageDTO(ExceptionUtils.E_INTERNAL_SERVER), HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @CrossOrigin
  @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<Object> findAll(
      @RequestParam(required = false) String name,
      @RequestParam(required = false) String type,
      @RequestParam(required = false) Boolean status,
      @Parameter(hidden = true) Pageable pageable) {
    try {
      SearchDTO dto=new SearchDTO();
      return new ResponseEntity<>(service.getAll(dto, pageable), HttpStatus.OK);
    } catch (Exception ex) {
      return new ResponseEntity<>(
          new MessageDTO(ExceptionUtils.E_INTERNAL_SERVER), HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  //  @CrossOrigin
  //  @GetMapping(value = "/drop", produces = MediaType.APPLICATION_JSON_VALUE)
  //  public ResponseEntity<Object> getDropDown() {
  //    try {
  //      return new ResponseEntity<>(service.getDropDown(), HttpStatus.OK);
  //    } catch (Exception ex) {
  //      return new ResponseEntity<>(
  //          new MessageDTO(ExceptionUtils.E_INTERNAL_SERVER), HttpStatus.INTERNAL_SERVER_ERROR);
  //    }
  //  }
}
