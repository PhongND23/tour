package com.saleweb.controllers;

import com.saleweb.common.MessageDTO;
import com.saleweb.common.SearchDTO;
import com.saleweb.exceptions.ApplicationException;
import com.saleweb.exceptions.ExceptionUtils;
import com.saleweb.models.category.PostCategoryCreateDTO;
import com.saleweb.models.category.PostCategoryUpdateDTO;
import com.saleweb.services.DashboardService;
import com.saleweb.services.PostCategoryService;
import io.swagger.v3.oas.annotations.Parameter;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RequiredArgsConstructor
@RequestMapping("/dashboard")
@RestController
public class DashboardController {
  private final DashboardService dashboardService;


  @CrossOrigin
  @GetMapping(value = "", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<Object> getDashboard() {
    try {
      return new ResponseEntity<>(dashboardService.getStatistical(), HttpStatus.OK);
    } catch (Exception ex) {
      log.error(ex.getMessage());
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

}
