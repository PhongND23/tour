package com.saleweb.domains;

import com.saleweb.domains.common.Auditable;
import com.saleweb.models.product.ProductCreateDTO;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
@Entity
@Table(name = "product")
public class Product extends Auditable {
  @Id
  @SequenceGenerator(
      name = "product_sequence_id",
      sequenceName = "product_sequence_id",
      allocationSize = 1)
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "product_sequence_id")
  @Column(name = "id")
  private Long id;

  @Column(name = "name")
  private String name;

  @Column(name = "category_id")
  private Long categoryId;

  @Column(name = "images")
  private String images;

  @Column(name = "price")
  private Double price;

  @Column(name = "promotion_price")
  private Double promotionPrice;

  @Column(name = "quantity")
  private Integer quantity;

  @Column(name = "description")
  private String description;

  @Column(name = "content")
  private String content;

  @Column(name = "view_count")
  private Integer viewCount;

  @Column(name = "status")
  private Boolean status;

  @Column(name = "classify")
  private String classify;

  @Column(name = "rate")
  private String rate;

  @Column(name = "trademark")
  private String trademark;

  @Column(name = "is_hot")
  private Boolean isHot;

  @Column(name = "like_count")
  private int likeCount;

  @Column(name = "tags")
  private String tags;

  public Product(ProductCreateDTO p) {

    this.name = p.getName();
    this.categoryId = p.getCategoryId();
    this.trademark = p.getTrademark();

    this.price = p.getPrice();
    this.promotionPrice = p.getPromotionPrice();
    this.classify = p.getClassify();
    this.quantity = p.getQuantity();
    this.tags=p.getTags();

    this.description = p.getDescription();
    this.content = p.getContent();
    this.status = Boolean.TRUE;
    this.likeCount = 0;
    this.viewCount = 0;

  }
}
