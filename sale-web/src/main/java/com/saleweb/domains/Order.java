package com.saleweb.domains;

import com.saleweb.domains.common.Auditable;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
@Entity
@Table(name = "orders")
public class Order extends Auditable {
  @Id
  @SequenceGenerator(
      name = "order_sequence_id",
      sequenceName = "order_sequence_id",
      allocationSize = 1)
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "order_sequence_id")
  @Column(name = "id")
  private Long id;

  @Column(name = "account_id")
  private Long accountId;

  @Column(name = "payment_id")
  private Long paymentId;

  @Column(name = "products")
  private String products;

  @Column(name = "status_id")
  private Long orderStatusId;

  @Column(name = "ship")
  private Double ship;

  public Order(Long accountId,Long paymentId,String products)
  {
    this.setAccountId(accountId);
    this.setPaymentId(paymentId);
    this.setOrderStatusId(1L);
    this.setProducts(products);
    this.setShip(1500000d);
  }
}
