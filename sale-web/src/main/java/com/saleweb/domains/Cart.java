package com.saleweb.domains;

import com.saleweb.domains.common.Auditable;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
@Entity
@Table(name = "cart")
public class Cart {
  @Id
  @SequenceGenerator(
      name = "cart_sequence_id",
      sequenceName = "cart_sequence_id",
      allocationSize = 1)
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "cart_sequence_id")
  @Column(name = "id")
  private Long id;


  @Column(name = "account_id")
  private Long accountId;

  public Cart(Long accountId)
  {
    this.setAccountId(accountId);
  }

}
