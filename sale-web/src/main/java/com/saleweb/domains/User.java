package com.saleweb.domains;

import com.saleweb.domains.common.Auditable;
import java.time.LocalDate;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.saleweb.models.user.UserCreateDTO;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
@Entity
@Table(name = "users")
public class User extends Auditable {
  @Id
  @SequenceGenerator(
      name = "users_sequence_id",
      sequenceName = "users_sequence_id",
      allocationSize = 1)
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "users_sequence_id")
  @Column(name = "id")
  private Long id;

  @Column(name = "fullname")
  private String fullName;

  @Column(name = "address")
  private String address;

  @Column(name = "email")
  private String email;

  @Column(name = "phone_number")
  private String phoneNumber;

  @Column(name = "gender")
  private Boolean gender;

  @Column(name = "date_of_birth")
  private LocalDate dateOfBirth;

  public User(UserCreateDTO dto)
  {
    this.setFullName(dto.getFullName());
    this.setAddress(dto.getAddress());
    this.setEmail(dto.getEmail());
    this.setPhoneNumber(dto.getPhoneNumber());
    this.setGender(dto.getGender());
    this.setDateOfBirth(dto.getDateOfBirth());


  }
}
