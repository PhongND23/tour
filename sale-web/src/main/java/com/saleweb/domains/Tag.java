package com.saleweb.domains;

import com.saleweb.domains.common.Auditable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
@Entity
@Table(name = "tag")
public class Tag extends Auditable {
  @Id
  @SequenceGenerator(name = "tag_sequence_id", sequenceName = "tag_sequence_id", allocationSize = 1)
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "tag_sequence_id")
  @Column(name = "id")
  private Long id;

  @Column(name = "status")
  private Boolean status;

  @Column(name = "name")
  private String name;

  @Column(name = "type")
  private String type;

  public Tag(String name)
  {
    this.setName(name);
    this.setStatus(true);
  }
}
