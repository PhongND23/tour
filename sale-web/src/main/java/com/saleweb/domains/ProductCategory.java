package com.saleweb.domains;

import com.saleweb.models.category.ProductCategoryCreateDTO;
import com.saleweb.domains.common.Auditable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
@Entity
@Table(name = "product_category")
public class ProductCategory extends Auditable {
  @Id
  @SequenceGenerator(
      name = "product_category_sequence_id",
      sequenceName = "product_category_sequence_id",
      allocationSize = 1)
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "product_category_sequence_id")
  @Column(name = "id")
  private Long id;

  @Column(name = "name", nullable = false)
  private String name;


  @Column(name = "description")
  private String description;

  @Column(name = "status")
  private Boolean status;

  public ProductCategory(ProductCategoryCreateDTO dto) {
    this.name = dto.getName();
    this.description = dto.getDescription();
    this.status = true;
  }
}
