package com.saleweb.domains;

import com.saleweb.models.cartdetail.CartDetailCreateDTO;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
@Entity
@Table(name = "cart_detail")
public class CartDetail {
  @Id
  @SequenceGenerator(
      name = "cart_detail_sequence_id",
      sequenceName = "cart_detail_sequence_id",
      allocationSize = 1)
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "cart_detail_sequence_id")
  @Column(name = "id")
  private Long id;


  @Column(name = "cart_id")
  private Long cartId;

  @Column(name = "product_id")
  private Long productId;

  @Column(name = "quantity")
  private Integer quantity;

  @Column(name = "price")
  private Double price;

  @Column(name = "promotion_price")
  private Double promotionPrice;

  @Column(name = "status")
  private Boolean status;


  public CartDetail(Long cartId,Long productId,Product product)
  {
    this.setCartId(cartId);
    this.setProductId(productId);
    this.setPrice(product.getPrice());
    this.setPromotionPrice(product.getPromotionPrice());
    this.setStatus(Boolean.FALSE);
    this.setQuantity(1);
  }
  public CartDetail(Long cartId,Long productId,Product product,int quantity)
  {
    this.setCartId(cartId);
    this.setProductId(productId);
    this.setPrice(product.getPrice());
    this.setPromotionPrice(product.getPromotionPrice());
    this.setStatus(Boolean.FALSE);
    this.setQuantity(quantity);
  }
}
