package com.saleweb.domains;

import com.saleweb.domains.common.Auditable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
@Entity
@Table(name = "order_status")
public class OrderStatus extends Auditable {
  @Id
  @SequenceGenerator(
      name = "order_status_sequence_id",
      sequenceName = "order_status_sequence_id",
      allocationSize = 1)
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "order_status_sequence_id")
  @Column(name = "id")
  private Long id;

  @Column(name = "name")
  private String name;
}
