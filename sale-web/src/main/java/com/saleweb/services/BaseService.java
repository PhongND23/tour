package com.saleweb.services;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.saleweb.common.SearchDTO;
import com.saleweb.exceptions.ApplicationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface BaseService<T, U, V> {
  void create(T dto) throws ApplicationException;

  void update(U dto, Long id) throws ApplicationException;

  void delete(Long id) throws ApplicationException;

  V findById(Long id) throws ApplicationException, JsonProcessingException;

  Page<V> getAll(SearchDTO dto, Pageable pageable) throws JsonProcessingException;
}
