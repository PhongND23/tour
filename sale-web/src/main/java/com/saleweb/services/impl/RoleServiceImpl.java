package com.saleweb.services.impl;

import com.saleweb.common.SearchDTO;
import com.saleweb.domains.Role;
import com.saleweb.exceptions.ApplicationException;
import com.saleweb.exceptions.ExceptionUtils;
import com.saleweb.models.category.DropDownDTO;
import com.saleweb.repositories.RoleRepository;
import com.saleweb.services.RoleService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.IterableUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Slf4j
public class RoleServiceImpl implements RoleService {

  private final RoleRepository repository;

  @Override
  public void create(Role dto) throws ApplicationException {
    repository.save(dto);
  }

  @Override
  public void update(Role dto, Long id) throws ApplicationException {
    Optional<Role> postCategory = repository.findById(id);
    if (postCategory.isEmpty()) {
      throw new ApplicationException(
          ExceptionUtils.E_CATEGORY_ID_NOT_EXIST,
          String.format(ExceptionUtils.messages.get(ExceptionUtils.E_CATEGORY_ID_NOT_EXIST), id));
    }
    if (StringUtils.isNotBlank(dto.getName())) {
      postCategory.get().setName(dto.getName());
    }
    if (StringUtils.isNotBlank(dto.getDescription())) {
      postCategory.get().setDescription(dto.getDescription());
    }
    if (dto.getStatus() != null) {
      postCategory.get().setStatus(dto.getStatus());
    }
    repository.save(postCategory.get());
  }

  @Override
  public void delete(Long id) throws ApplicationException {
    if (!repository.existsById(id)) {
      throw new ApplicationException(
          ExceptionUtils.E_CATEGORY_ID_NOT_EXIST,
          String.format(ExceptionUtils.messages.get(ExceptionUtils.E_CATEGORY_ID_NOT_EXIST), id));
    }
    repository.deleteById(id);
  }

  @Override
  public Role findById(Long id) throws ApplicationException {
    Optional<Role> byId = repository.findById(id);
    if (byId.isEmpty()) {
      throw new ApplicationException(
          ExceptionUtils.E_CATEGORY_ID_NOT_EXIST,
          String.format(ExceptionUtils.messages.get(ExceptionUtils.E_CATEGORY_ID_NOT_EXIST), id));
    }
    return byId.get();
  }

  @Override
  public Page<Role> getAll(SearchDTO dto, Pageable pageable) {
    Sort sort;
    sort = pageable.getSort();
    pageable = PageRequest.of(pageable.getPageNumber(), pageable.getPageSize(), sort);
    Specification<Role> specification =
        (root, criteriaQuery, criteriaBuilder) -> {
          List<Predicate> predicates = new ArrayList<>();
          if (StringUtils.isNotBlank(dto.getName())) {
            predicates.add(criteriaBuilder.equal(root.get("name"), dto.getName()));
          }

          if (null != dto.getStatus()) {
            predicates.add(criteriaBuilder.equal(root.get("status"), dto.getStatus()));
          }
          return criteriaQuery.where(predicates.toArray(new Predicate[0])).getRestriction();
        };
    Page<Role> all = repository.findAll(specification, pageable);
    return repository.findAll(pageable);
  }

  @Override
  public List<DropDownDTO> getDrop() {
    return IterableUtils.toList(repository.findAll()).stream().map(DropDownDTO::new).collect(Collectors.toList());
  }
}
