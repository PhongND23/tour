package com.saleweb.services.impl;

import com.saleweb.common.SearchDTO;
import com.saleweb.domains.Tag;
import com.saleweb.exceptions.ApplicationException;
import com.saleweb.exceptions.ExceptionUtils;
import com.saleweb.repositories.TagRepository;
import com.saleweb.services.TagService;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import javax.persistence.criteria.Predicate;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Slf4j
public class TagServiceImpl implements TagService {

  private final TagRepository repository;

  @Override
  public void create(Tag dto) throws ApplicationException {
    // check name
    if (StringUtils.isBlank(dto.getName())) {
      throw new ApplicationException(
          ExceptionUtils.E_POST_NAME_NULL,
          ExceptionUtils.messages.get(ExceptionUtils.E_POST_NAME_NULL));
    }
    // check type
    if (StringUtils.isBlank(dto.getType())) {
      throw new ApplicationException(
          ExceptionUtils.E_TYPE_NULL, ExceptionUtils.messages.get(ExceptionUtils.E_TYPE_NULL));
    }
    repository.save(dto);
  }

  @Override
  public void update(Tag dto, Long id) throws ApplicationException {
    Optional<Tag> byId = repository.findById(id);
    if (byId.isEmpty()) {
      throw new ApplicationException(
          ExceptionUtils.E_POST_ID_NOT_EXISTS,
          String.format(ExceptionUtils.messages.get(ExceptionUtils.E_POST_ID_NOT_EXISTS), id));
    }
    if (StringUtils.isNotBlank(dto.getName())) {
      byId.get().setName(dto.getName());
    }
    if (null != dto.getStatus()) {
      byId.get().setStatus(dto.getStatus());
    }
    if (StringUtils.isNotBlank(dto.getType())) {
      byId.get().setType(dto.getType());
    }
    repository.save(byId.get());
  }

  @Override
  public void delete(Long id) throws ApplicationException {
    Optional<Tag> byId = repository.findById(id);
    if (byId.isEmpty()) {
      throw new ApplicationException(
          ExceptionUtils.E_POST_ID_NOT_EXISTS,
          String.format(ExceptionUtils.messages.get(ExceptionUtils.E_POST_ID_NOT_EXISTS), id));
    }
    repository.delete(byId.get());
  }

  @Override
  public Tag findById(Long id) throws ApplicationException {
    Optional<Tag> byId = repository.findById(id);
    if (byId.isEmpty()) {
      throw new ApplicationException(
          ExceptionUtils.E_POST_ID_NOT_EXISTS,
          String.format(ExceptionUtils.messages.get(ExceptionUtils.E_POST_ID_NOT_EXISTS), id));
    }
    return byId.get();
  }

  @Override
  public Page<Tag> getAll(SearchDTO dto, Pageable pageable) {
    Sort sort;
    sort = pageable.getSort();
    pageable = PageRequest.of(pageable.getPageNumber(), pageable.getPageSize(), sort);
    Specification<Tag> specification =
        (root, criteriaQuery, criteriaBuilder) -> {
          List<Predicate> predicates = new ArrayList<>();
          if (StringUtils.isNotBlank(dto.getName())) {
            predicates.add(criteriaBuilder.like(root.get("name"), dto.getName()));
          }
          if (StringUtils.isNotBlank(dto.getType())) {
            predicates.add(criteriaBuilder.equal(root.get("type"), dto.getType()));
          }
          if (null != dto.getStatus()) {
            predicates.add(criteriaBuilder.equal(root.get("status"), dto.getStatus()));
          }
          return criteriaQuery.where(predicates.toArray(new Predicate[0])).getRestriction();
        };
    Page<Tag> all = repository.findAll(specification, pageable);
    return new PageImpl<>(all.getContent(), pageable, all.getTotalElements());
  }
}
