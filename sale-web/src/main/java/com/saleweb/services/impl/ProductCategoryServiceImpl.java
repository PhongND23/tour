package com.saleweb.services.impl;

import com.saleweb.common.SearchDTO;
import com.saleweb.models.category.DropDownDTO;
import com.saleweb.models.category.ProductCategoryCreateDTO;
import com.saleweb.models.category.ProductCategoryUpdateDTO;
import com.saleweb.domains.ProductCategory;
import com.saleweb.exceptions.ApplicationException;
import com.saleweb.exceptions.ExceptionUtils;
import com.saleweb.repositories.ProductCategoryRepository;
import com.saleweb.services.ProductCategoryService;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import javax.persistence.criteria.Predicate;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.IterableUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Slf4j
public class ProductCategoryServiceImpl implements ProductCategoryService {

  private final ProductCategoryRepository repository;

  @Override
  public void create(ProductCategoryCreateDTO dto) throws ApplicationException {
    if (StringUtils.isBlank(dto.getName())) {
      throw new ApplicationException(
          ExceptionUtils.E_CATEGORY_NAME_NULL,
          ExceptionUtils.messages.get(ExceptionUtils.E_CATEGORY_ID_NOT_EXIST));
    }
    repository.save(new ProductCategory(dto));
  }

  @Override
  public void update(ProductCategoryUpdateDTO dto, Long id) throws ApplicationException {
    Optional<ProductCategory> productCategory = repository.findById(id);
    if (productCategory.isEmpty()) {
      throw new ApplicationException(
          ExceptionUtils.E_CATEGORY_ID_NOT_EXIST,
          String.format(ExceptionUtils.messages.get(ExceptionUtils.E_CATEGORY_ID_NOT_EXIST), id));
    }
    if (StringUtils.isNotBlank(dto.getName())) {
      productCategory.get().setName(dto.getName());
    }
    productCategory.get().setName(dto.getName());

    repository.save(productCategory.get());
  }

  @Override
  public void delete(Long id) throws ApplicationException {
    if (!repository.existsById(id)) {
      throw new ApplicationException(
          ExceptionUtils.E_CATEGORY_ID_NOT_EXIST,
          String.format(ExceptionUtils.messages.get(ExceptionUtils.E_CATEGORY_ID_NOT_EXIST), id));
    }
    repository.deleteById(id);
  }

  @Override
  public ProductCategory findById(Long id) throws ApplicationException {
    Optional<ProductCategory> byId = repository.findById(id);
    if (byId.isEmpty()) {
      throw new ApplicationException(
          ExceptionUtils.E_CATEGORY_ID_NOT_EXIST,
          String.format(ExceptionUtils.messages.get(ExceptionUtils.E_CATEGORY_ID_NOT_EXIST), id));
    }
    return byId.get();
  }

  @Override
  public Page<ProductCategory> getAll(SearchDTO dto, Pageable pageable) {
    Sort sort;
    sort = pageable.getSort();
    pageable = PageRequest.of(pageable.getPageNumber(), pageable.getPageSize(), sort);
    Specification<ProductCategory> specification =
        (root, criteriaQuery, criteriaBuilder) -> {
          List<Predicate> predicates = new ArrayList<>();
          if (StringUtils.isNotBlank(dto.getName())) {
            predicates.add(criteriaBuilder.like(root.get("name"), "%"+dto.getName()+"%"));
          }

          return criteriaQuery.where(predicates.toArray(new Predicate[0])).getRestriction();
        };

    return repository.findAll(specification, pageable);
  }

  @Override
  public List<DropDownDTO> getDropDown() {
    return IterableUtils.toList(repository.findAll()).stream()
        .map(DropDownDTO::new)
        .collect(Collectors.toList());
  }
}
