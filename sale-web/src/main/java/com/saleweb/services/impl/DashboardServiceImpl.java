package com.saleweb.services.impl;

import com.saleweb.models.dashboard.DashboardDTO;
import com.saleweb.repositories.*;
import com.saleweb.services.DashboardService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.IterableUtils;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Slf4j
public class DashboardServiceImpl implements DashboardService {

    private final ProductRepository productRepository;
    private final PostRepository postRepository;
    private final ProductCategoryRepository productCategoryRepository;
    private final PostCategoryRepository postCategoryRepository;
    private final OrderRepository orderRepository;
    private final AccountRepository accountRepository;
    @Override
    public DashboardDTO getStatistical() {
       var orderCount= IterableUtils.toList(orderRepository.findAll()).size();
       var accountCount= IterableUtils.toList(accountRepository.findAll()).size();
       var productCategoryCount= IterableUtils.toList(productCategoryRepository.findAll()).size();
       var postCategoryCount= IterableUtils.toList(postCategoryRepository.findAll()).size();
       var productCount= IterableUtils.toList(productRepository.findAll()).size();
       var postCount= IterableUtils.toList(postRepository.findAll()).size();

        return new DashboardDTO(orderCount,accountCount,productCategoryCount,postCategoryCount,productCount,postCount);
    }
}
