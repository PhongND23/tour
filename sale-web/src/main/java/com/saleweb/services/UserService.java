package com.saleweb.services;

import com.saleweb.models.user.UserCreateDTO;
import com.saleweb.models.user.UserDTO;
import com.saleweb.models.user.UserUpdateDTO;

public interface UserService extends BaseService<UserCreateDTO, UserUpdateDTO, UserDTO> {}
