package com.saleweb.services;

import com.saleweb.domains.Role;
import com.saleweb.models.category.DropDownDTO;
import com.saleweb.models.dashboard.DashboardDTO;

import java.util.List;

public interface DashboardService {

    DashboardDTO getStatistical();
}
