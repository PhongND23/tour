package com.saleweb.services;

import com.saleweb.domains.Post;
import com.saleweb.models.post.PostCreateDTO;
import com.saleweb.models.post.PostDTO;
import com.saleweb.models.post.PostUpdateDTO;

public interface PostService extends BaseService<PostCreateDTO, PostUpdateDTO, PostDTO> {}
