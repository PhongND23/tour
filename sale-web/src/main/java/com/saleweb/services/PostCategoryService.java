package com.saleweb.services;

import com.saleweb.models.category.DropDownDTO;
import com.saleweb.models.category.PostCategoryCreateDTO;
import com.saleweb.domains.PostCategory;
import com.saleweb.models.category.PostCategoryDTO;
import com.saleweb.models.category.PostCategoryUpdateDTO;

import java.util.List;

public interface PostCategoryService
    extends BaseService<PostCategoryCreateDTO, PostCategoryUpdateDTO, PostCategoryDTO> {
    List<DropDownDTO> getDropDown();
}
