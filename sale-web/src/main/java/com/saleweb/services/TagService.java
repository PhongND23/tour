package com.saleweb.services;

import com.saleweb.domains.Tag;

public interface TagService extends BaseService<Tag, Tag, Tag> {}
